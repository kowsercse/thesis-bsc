/*
 * OuterSwitch.java
 *
 * Created on November 19, 2007, 2:50 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pioneer.switches;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import pioneer.settings.Settings;

/**
 *
 * @author kowser
 */
public class OuterSwitch extends Polygon {
    
    /** Creates a new instance of OuterSwitch */
    public OuterSwitch(int centerX, int centerY) {
        xCenter = centerX;
        yCenter = centerY;
        super.xpoints = new int[] {xCenter, xCenter - xDist, xCenter - xDist};
        super.ypoints = new int[] {yCenter, yCenter + yDist, yCenter - yDist};
        super.npoints = 3;
        inputPoint[0] = new Point(super.xpoints[2], super.ypoints[2]+Settings.SWITCH_PORT_DIST);
        inputPoint[1] = new Point(super.xpoints[1], super.ypoints[1]-Settings.SWITCH_PORT_DIST);
        outputPoint = new Point(super.xpoints[0], super.ypoints[0]);
    }

    public Point getInputPoint(int inputNo) {
        return inputPoint[inputNo];
    }

    public Point getOutputPoint() {
        return outputPoint;
    }

    public void drawSwitch(Graphics graphics) {
        graphics.setColor(color);
        graphics.drawPolygon(this);
        graphics.fillPolygon(this);
        graphics.setColor(new Color(0));
    }

    public void setColor(Color c) {
        if(Settings.DEBUG == false) {
            if(c == Settings.DEFAULT_COLOR)
                color = Settings.DEFAULT_COLOR;
            else if(color == Settings.DEFAULT_COLOR)
                color = c;
            else
                color = Settings.CROSSTALK_COLOR;
        } else
            color = c;
    }

    public Color getColor() {
        return color;
    }

    private final int xDist = Settings.SWITCH_HALF_WIDTH;
    private final int yDist = Settings.SWITCH_HALF_HEIGHT;
    private final int xCenter;
    private final int yCenter;
    private Color color = Settings.DEFAULT_COLOR;
    
    private Point inputPoint[] = new Point[2];
    private Point outputPoint;

}
