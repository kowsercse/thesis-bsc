/*
 * ContainerPanel.java
 *
 * Created on November 19, 2007, 11:22 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pioneer;

import java.awt.Graphics;
import javax.swing.JPanel;
import pioneer.switchingnetwork.Network;

/**
 *
 * @author kowser
 */
public class ContainerPanel extends JPanel {
    
    /** Creates a new instance of ContainerPanel */
    public ContainerPanel() {
        super();
    }
    
    protected void paintComponent(Graphics graphics) {
        super.paintComponents(graphics);
        if(network != null)
            network.drawNetwork(graphics);
    }
    
    public void setNetwork(Network newNetwork) {
        network = newNetwork;
        repaint();
    }

    private Network network;

}
