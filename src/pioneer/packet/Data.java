/*
 * Data.java
 *
 * Created on February 14, 2008, 12:18 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pioneer.packet;

import java.awt.Color;
import pioneer.settings.Settings;

/**
 *
 * @author kowser
 */
public class Data {
    
    /** Creates a new instance of Data */
    public Data() {
        sourcePort = destinationPort = -1;
        color = Settings.DEFAULT_COLOR;
    }
    
    public Data(int src, int dest) {
        sourcePort = src;
        destinationPort = dest;
        color = Settings.DATA_COLOR[sourcePort];
    }

    public Data(int src, int dest, Color clr) {
        sourcePort = src;
        destinationPort = dest;
        color = clr;
    }
    
    public int getSourcePort() {
        return sourcePort;
    }

    public int getDestinationPort() {
        return destinationPort;
    }
    
    public Color getColor() {
        return color;
    }

    private int sourcePort;
    private int destinationPort;
    private Color color;
    
}
