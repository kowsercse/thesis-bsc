/*
 * Settings.java
 * 
 * Created on Nov 6, 2007, 10:05:46 PM
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pioneer.settings;

import java.awt.Color;

/**
 *
 * @author kowser
 */
public class Settings {

    public Settings() {
    }
    public static int SWITCH_HALF_WIDTH = 15;
    public static int SWITCH_HALF_HEIGHT = 10;
    public static int SWITCH_PORT_DIST = 4;
    
    public static int MIN_WIDTH = 350;
    public static int MIN_HEIGHT = 500;
    
    public static int MIDDLE_STAGE_WIDTH = 100;    //375 to 600
    //public static int MID_STAGE_HEIGHT = 1150;   //50 to 1200
    
    public static Color DEFAULT_COLOR = new Color(0, 125, 0);
    public static Color CORSSTALK_COLOR = Color.BLUE;
    public static Color BLOCKING_COLOR = Color.RED;
    public static Color DATA_COLOR[] = {
        new Color( 75,  75, 255), new Color( 75,   0,  75), new Color(  0,  75,  75), new Color( 255,  255, 255),
        new Color(100, 100,   0), new Color(100, 255, 100), new Color(  0, 100, 100), new Color(100, 100, 100),
        new Color(125, 125,   0), new Color(125,   0, 125), new Color(255, 125, 125), new Color(125, 125, 125),
        new Color(150, 150,   0), new Color(150,   0, 150), new Color(  0, 150, 150), new Color(150, 150, 150),
        new Color(175, 175,   0), new Color(175,   0, 175), new Color(  0, 175, 175), new Color(175, 175, 175),
        new Color(200, 200,   0), new Color(200,   0, 200), new Color(  0, 200, 200), new Color(200, 200, 200),
        new Color(225, 225,   0), new Color(225,   0, 225), new Color(  0, 225, 225), new Color(225, 225, 225),
        new Color(250, 250,   0), new Color(250,   0, 250), new Color(  0, 250, 250), new Color(250, 250, 250),
//        new Color(  0, 255, 255)
    };

    public static Color CROSSTALK_COLOR = new Color(255, 0, 0);
    public static boolean DEBUG = false;

    public static double log2(double value) {
        return Math.log10(value)/Math.log10(2);
    }
    
}
