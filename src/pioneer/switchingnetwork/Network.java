/*
 * Network.java
 *
 * Created on November 27, 2007, 6:55 PM
 *
 */

package pioneer.switchingnetwork;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import pioneer.*;
import pioneer.packet.Data;
import pioneer.settings.Settings;
import pioneer.switches.*;

/**
 *
 * @author kowser
 */
public class Network {
    
    /** Creates a new instance of Network */
    public Network(int n) {
        networkSize = n;
        if(networkSize == 4) {
            basicNetwork = true;
            middleStageSwitch2by2 = new Switch2By2[2];
        } else {
            basicNetwork = false;
            subNetwork = new Network[2];
            subNetwork[0] = new Network(networkSize/2);
            subNetwork[1] = new Network(networkSize/2);
        }
        inputSwitch2by2 = new Switch2By2[networkSize/2];
        outputSwitch2by2 = new Switch2By2[networkSize/2];
        inputConnectionLine = new ConnectionLine[networkSize];
        outputConnectionLine = new ConnectionLine[networkSize];
        totalStage = 2*(int)Settings.log2(networkSize)-1;
        forwardPermutationData = new Data[networkSize/2];
        backwardPermutationData = new Data[networkSize/2];
    }
    
    // <editor-fold defaultstate="collapsed" desc=" initNetworkSwitch ">
    public void initNetworkSwitch(int leftTopX, int leftTopY, int rightBottomX, int rightBottomY) {
        int width = rightBottomX - leftTopX;
        int height = rightBottomY - leftTopY;
        
        int incY = height/(networkSize/2-1);
        int incX = width/(totalStage-1);
        
        int leftCenterX = leftTopX;
        int rightCenterX = rightBottomX;
        int centerY = leftTopY;
        
        for (int row = 0; row < networkSize/2; row++) {
            inputSwitch2by2[row] = new Switch2By2(leftCenterX, centerY);
            outputSwitch2by2[row] = new Switch2By2(rightCenterX, centerY);
            centerY += incY;
        }
        if(basicNetwork == false) {
            int mid = leftTopY + incY*networkSize/4;
            subNetwork[0].initNetworkSwitch(leftCenterX+incX, leftTopY, rightBottomX-incX, mid-incY);
            subNetwork[1].initNetworkSwitch(leftCenterX+incX, mid, rightBottomX-incX, rightBottomY);
        } else {
            middleStageSwitch2by2[0] = new Switch2By2(leftCenterX+incX, leftTopY);
            middleStageSwitch2by2[1] = new Switch2By2(leftCenterX+incX, rightBottomY);
        }
    }// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" initNetworkConnectionLine ">
    public void initNetworkConnectionLine() {
        if(basicNetwork == false) {
            int shift = (int)Math.pow(2, (totalStage+1)/2-3);
            //System.out.println("Shift: "+shift+" total Stage: "+totalStage);
            for (int i = 0; i < networkSize; i+=2) {
                int dest = (i - 2*shift + 1 + networkSize) % networkSize;
                inputConnectionLine[i]  = new ConnectionLine(inputSwitch2by2[i/2].getOutputPoint(0),
                        subNetwork[2*dest/networkSize].getInputPoint(dest%(networkSize/2)));
                outputConnectionLine[i] = new ConnectionLine(outputSwitch2by2[i/2].getInputPoint(0),
                        subNetwork[2*dest/networkSize].getOutputPoint(dest%(networkSize/2)));
                
                dest = (i + 2*shift + networkSize) % networkSize;
                inputConnectionLine[i+1]  = new ConnectionLine(inputSwitch2by2[i/2].getOutputPoint(1),
                        subNetwork[2*dest/networkSize].getInputPoint(dest%(networkSize/2)));
                outputConnectionLine[i+1] = new ConnectionLine(outputSwitch2by2[i/2].getInputPoint(1),
                        subNetwork[2*dest/networkSize].getOutputPoint(dest%(networkSize/2)));
            }
            subNetwork[0].initNetworkConnectionLine();
            subNetwork[1].initNetworkConnectionLine();
        } else {
            inputConnectionLine[0]  = new ConnectionLine(inputSwitch2by2[0].getOutputPoint(0), middleStageSwitch2by2[0].getInputPoint(0));
            outputConnectionLine[0] = new ConnectionLine(outputSwitch2by2[0].getInputPoint(0), middleStageSwitch2by2[0].getOutputPoint(0));
            
            inputConnectionLine[1] = new ConnectionLine(inputSwitch2by2[0].getOutputPoint(1), middleStageSwitch2by2[1].getInputPoint(0));
            outputConnectionLine[1] = new ConnectionLine(outputSwitch2by2[0].getInputPoint(1), middleStageSwitch2by2[1].getOutputPoint(0));
            
            inputConnectionLine[2] = new ConnectionLine(inputSwitch2by2[1].getOutputPoint(0), middleStageSwitch2by2[0].getInputPoint(1));
            outputConnectionLine[2] = new ConnectionLine(outputSwitch2by2[1].getInputPoint(0), middleStageSwitch2by2[0].getOutputPoint(1));
            
            inputConnectionLine[3] = new ConnectionLine(inputSwitch2by2[1].getOutputPoint(1), middleStageSwitch2by2[1].getInputPoint(1));
            outputConnectionLine[3] = new ConnectionLine(outputSwitch2by2[1].getInputPoint(1), middleStageSwitch2by2[1].getOutputPoint(1));
        }
    }// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Draw Network ">
    public void drawNetwork(Graphics graphics) {
        for (int  i = 0;  i < networkSize/2;  i++) {
            inputSwitch2by2[i].drawSwitch(graphics);
            outputSwitch2by2[i].drawSwitch(graphics);
        }
        Graphics2D graphics2D = (Graphics2D) graphics;
        for (int i = 0; i < networkSize; i++) {
            graphics2D.draw(inputConnectionLine[i]);
            graphics2D.draw(outputConnectionLine[i]);
        }
        if(basicNetwork == false) {
            subNetwork[0].drawNetwork(graphics);
            subNetwork[1].drawNetwork(graphics);
        } else {
            middleStageSwitch2by2[0].drawSwitch(graphics);
            middleStageSwitch2by2[1].drawSwitch(graphics);
        }
    }// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" get Input Output Point ">
    public Point getInputPoint(int inputNo) {
        if(inputNo%2 == 0)
            return inputSwitch2by2[inputNo/2].getInputPoint(0);
        else
            return inputSwitch2by2[inputNo/2].getInputPoint(1);
    }
    
    public Point getOutputPoint(int outputNo) {
        if(outputNo%2 == 0)
            return outputSwitch2by2[outputNo/2].getOutputPoint(0);
        else
            return outputSwitch2by2[outputNo/2].getOutputPoint(1);
    }// </editor-fold>
    
    public void simulateNetwork(int[] destination) {
        int[] edgeList = generateBipartiteGraph(destination);
        for (int i = 0; i < networkSize/2; i++) {
            forwardPermutationData[i] = new Data();
            backwardPermutationData[i] = new Data();
        }
        for (int i = 0; i < networkSize; i++) {
            if(edgeList[i] == 1)
                forwardPermutationData[i/2] = new Data(i/2, destination[i]/2, Settings.DATA_COLOR[i]);
            else if(edgeList[i] == -1)
                backwardPermutationData[i/2] = new Data(i/2, destination[i]/2, Settings.DATA_COLOR[i]);
        }
        System.out.println("<Forward permutaion according to source>");
        for (int i = 0; i < networkSize; i++) {
            if(edgeList[i] == 1)
                System.out.println("\t" + i + "---" + destination[i]);
        }
        System.out.println("\n<Backward permutaion according to source>");
        for (int i = 0; i < networkSize; i++) {
            if(edgeList[i] == -1)
                System.out.println("\t" + i + "---" + destination[i]);
        }
        System.out.println("\n<Realising forward permutaion>");
        routeSemiPermutation(forwardPermutationData);
    }
    
    public void routeSemiPermutation(Data[] data) {
        for (int i = 0; i < networkSize/2; i++) {
            if(data[i].getSourcePort() != -1) {
                inputSwitch2by2[i].setColor(data[i].getColor());
                outputSwitch2by2[data[i].getDestinationPort()].setColor(data[i].getColor());
                //System.out.println("["+data[i].getSourcePort()+" "+data[i].getDestinationPort() + "]");
            }
        }
        if(basicNetwork == false) {
            int[] destination = defaultIntArray(networkSize/2, -1);
            for (int i = 0; i < networkSize/2; i++) {
                destination[i] = data[i].getDestinationPort();
            }
            int[] combinedPermutation = generateCombinedPermutation(destination);
            int[] edgeList = generateBipartiteGraph(combinedPermutation);
            Data[] upperData = new Data[networkSize/4], lowerData = new Data[networkSize/4];
            for (int i = 0; i < networkSize/4; i++) {
                upperData[i] = new Data();
                lowerData[i] = new Data();
            }
            for (int i = 0; i < networkSize/2; i++) {
                if(edgeList[i] == 1) {
                    upperData[i/2] = new Data(i/2, combinedPermutation[i]/2, data[permutaionSource[i]].getColor());
                } else if(edgeList[i] == -1) {
                    lowerData[i/2] = new Data(i/2, combinedPermutation[i]/2, data[permutaionSource[i]].getColor());
                }
            }
/**/
            System.out.println("---------------------");
            System.out.println("Forward Permutaion:");
            for (int i = 0; i < networkSize/2; i++) {
                if(edgeList[i] == 1)
                    System.out.println("["+i/2+" "+combinedPermutation[i]/2 + "] " + permutaionSource[i]);
            }
            System.out.println("Backward Permutaion:");
            for (int i = 0; i < networkSize/2; i++) {
                if(edgeList[i] == -1)
                    System.out.println("["+i/2+" "+combinedPermutation[i]/2 + "] " + permutaionSource[i]);
            }
            System.out.println("*********************");
/**/
            subNetwork[0].routeSemiPermutation(upperData);
            subNetwork[1].routeSemiPermutation(lowerData);
        }
    }

    public void realizePermutation(int permutaionID) {
        if(permutaionID == 1) {
            System.out.println("\n<Realising forward permutaion>");
            routeSemiPermutation(forwardPermutationData);
        } else if(permutaionID == 2) {
            System.out.println("\n<Realising backward permutaion>");
            routeSemiPermutation(backwardPermutationData);
        }
    }
    
    private int[] generateCombinedPermutation(int[] destination) {
        int[] combinedPermutation = defaultIntArray(networkSize/2, -1);
        int src, dest;
        int shift = (int)Math.pow(2, (totalStage+1)/2-3);
        boolean[] usedSrc = new boolean[networkSize/4];
        boolean[] usedDest = new boolean[networkSize/4];
        permutaionSource = defaultIntArray(networkSize/2, -1);

        for (int i = 0; i < networkSize/2; i++) {
            if(destination[i] != -1) {
                src = (i + shift + networkSize/2) % (networkSize/4);
                dest = (destination[i] + shift + networkSize/2) % (networkSize/4);
//                System.out.println("--->[ " + i+":"+src + " " + destination[i]+":"+dest + " ]");
                if(usedSrc[src] == false) {
                    usedSrc[src] = true;
                    if(usedDest[dest] == false) {
                        usedDest[dest] = true;
                        combinedPermutation[2*src] = dest*2;
                    } else {
                        combinedPermutation[2*src] = dest*2 + 1;
                    }
                    permutaionSource[2*src] = i;
                }
                else {
                    if(usedDest[dest] == false) {
                        usedDest[dest] = true;
                        combinedPermutation[2*src+1] = dest*2;
                    } else {
                        combinedPermutation[2*src+1] = dest*2 + 1;
                    }
                    permutaionSource[2*src + 1] = i;
                }
            }
        }
        return combinedPermutation;
    }
    
    // <editor-fold defaultstate="collapsed" desc=" Click To Expand ">
    public void refresh() {
        for (int i = 0; i < networkSize/2; i++) {
            inputSwitch2by2[i].refresh();
            outputSwitch2by2[i].refresh();
        }
        if(basicNetwork == false) {
            subNetwork[0].refresh();
            subNetwork[1].refresh();
        } else {
            for (int i = 0; i < networkSize/2; i++) {
                middleStageSwitch2by2[i].refresh();
            }
        }
    }
    
    private int[] generateBipartiteGraph(int[] destination) {
        int[] source = defaultIntArray(destination.length, -1);
        for (int i = 0; i < destination.length; i++) {
            if(destination[i] != -1)
                source[destination[i]] = i;
        }
        int[] edgeList = new int[destination.length];
        int startNode, src, dest;
        for (int i = 0; i < destination.length; i++) {
            if(edgeList[i] == 0) {
                int endNode = -1;
                if(edgeList[adjacent(i)] == 0) {
                    startNode = src = i;
                    while(startNode != endNode) {
                        dest = destination[src];
                        if(dest == -1)
                            break;
                        edgeList[src] = 1;  //marked forward
                        src = source[adjacent(dest)];
                        if(src == -1)
                            break;
                        edgeList[src] = -1; //marked backward
                        endNode = src = adjacent(src);
                    }
                } else {
                    startNode = src = i;
                    while(startNode != endNode) {
                        dest = destination[src];
                        if(dest == -1)
                            break;
                        edgeList[src] = -1;  //marked backward
                        src = source[adjacent(dest)];
                        if(src == -1)
                            break;
                        edgeList[src] = 1;  //marked forward
                        endNode = src = adjacent(src);
                    }
                }
            }
        }
        return edgeList;
    }
    
    private int adjacent(int value) {
        if(value%2 == 0)
            return value+1;
        else
            return value-1;
    }
    
    private int[] defaultIntArray(int size, int defaultValue) {
        int[] integerArray = new int[size];
        for (int i = 0; i < size; i++) {
            integerArray[i] = defaultValue;
        }
        return integerArray;
    }// </editor-fold>

    private int networkSize;
    private int totalStage;
    private boolean basicNetwork;
    private int[] permutaionSource;
    
    private Data[] forwardPermutationData;
    private Data[] backwardPermutationData;
    
    private Switch2By2[] inputSwitch2by2;
    private Switch2By2[] outputSwitch2by2;
    private Switch2By2[] middleStageSwitch2by2;
    private ConnectionLine[] inputConnectionLine;
    private ConnectionLine[] outputConnectionLine;
    
    private Network[] subNetwork;

}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc=" Generated Code ">
