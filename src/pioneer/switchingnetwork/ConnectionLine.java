/*
 * ConnectionLine.java
 *
 * Created on November 23, 2007, 4:04 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pioneer.switchingnetwork;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author kowser
 */
public class ConnectionLine extends Line2D {
    
    /** Creates a new instance of ConnectionLine */
    public ConnectionLine() {
        //this.setLine(0, 0, 0, 0);
    }

    public ConnectionLine(Point start, Point end) {
        this.setLine(start.getX(), start.getY(), end.getX(), end.getY());
        //System.out.println(start.getX()+" "+start.getY()+" "+end.getX()+" "+end.getY());
    }
    
    public double getX1() {
        return x1;
    }

    public double getY1() {
        return y1;
    }

    public Point2D getP1() {
        return new Point2D.Float(x1, y1);
    }

    public double getX2() {
        return x2;
    }

    public double getY2() {
        return y2;
    }

    public Point2D getP2() {
        return new Point2D.Float(x2, y2);
    }

    public void setLine(double x1, double y1, double x2, double y2) {
        this.x1 = (float)x1;
        this.y1 = (float)y1;
        this.x2 = (float)x2;
        this.y2 = (float)y2;
    }

    public Rectangle2D getBounds2D() {
	    float x, y, w, h;
	    if (x1 < x2) {
		x = x1;
		w = x2 - x1;
	    } else {
		x = x2;
		w = x1 - x2;
	    }
	    if (y1 < y2) {
		y = y1;
		h = y2 - y1;
	    } else {
		y = y2;
		h = y1 - y2;
	    }
	    return new Rectangle2D.Float(x, y, w, h);
    }

    private float x1;
    private float x2;
    private float y1;
    private float y2;

}
