/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pioneer;

import java.io.File;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Sumel
 */
public class InputTableModel extends AbstractTableModel{

    public InputTableModel(int col) {
        column = col;
        inputNumUsed = new boolean[column];
        outputNumUsed = new boolean[column];
        outputPort = new Object[column];
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount() {
        return column;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return outputPort[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return Integer.toString(column);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if(aValue.equals("") == true) {
            outputPort[columnIndex] = null;
            return;
        }
        int value = Integer.parseInt(aValue.toString());
        if(outputNumUsed[value] == false){
            outputPort[columnIndex] = aValue;
            outputNumUsed[value] = true;
        }
        else if(outputNumUsed[value] == true){
            javax.swing.JOptionPane.showMessageDialog(null, "The output port is already selected!!!", 
                    "Wrong selection", JOptionPane.ERROR_MESSAGE);
        }        
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
	return true;
    }

    private void clearInOutNumUsed(){
        inputNumUsed = new boolean[column];
        outputNumUsed = new boolean[column];
        outputPort = new Object[column];
    }    
    
    public void generateInputToOutput(int numOfInput){
        clearInOutNumUsed();
        Random rand = new Random();        
        int inputNumber = 0;
        int outputNumber = 0;
        //System.out.println("Number of Input selected : " + numOfInput);
        for(int i = 0; i < numOfInput; i++){            
            inputNumber = rand.nextInt(column);
            if(inputNumUsed[inputNumber] == true){
                i--;
                continue;
            }
            inputNumUsed[inputNumber] = true;
            //System.out.println("Input num -> " + inputNumber);
            
            outputNumber = rand.nextInt(column);
            while(outputNumUsed[outputNumber] == true){
                outputNumber = rand.nextInt(column);
            }
            //System.out.println("Output num -> " + outputNumber);
            setValueAt(outputNumber, 0, inputNumber);
        }        
        fireTableStructureChanged();
    }
    
    public int[] getDestination() {
        int destination[] = new int[column];
        for (int i = 0; i < column; i++) {
            try {
                destination[i] = Integer.parseInt(outputPort[i].toString());
            } catch(Exception ex) {
                destination[i] = -1;
            }
        }
        return (int[]) destination.clone();
    }
    
    public void clear(){
        clearInOutNumUsed();
        fireTableStructureChanged();
    }
    
    private boolean inputNumUsed[];
    private boolean outputNumUsed[];
    private Object outputPort[];
    private int column;
    
}
