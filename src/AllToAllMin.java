/*
 * AllToAllMin.java
 *
 * Created on November 19, 2007, 2:34 PM
 *
 */

package pioneer.switchingnetwork;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import pioneer.packet.Data;
import pioneer.settings.Settings;
import pioneer.switches.InnerSwitch;
import pioneer.switches.OuterSwitch;
import pioneer.switches.Switch2By2;

/**
 *
 * @author kowser
 */
public class AllToAllMin {
    
    /** Creates a new instance of AllToAllMin */
    // <editor-fold defaultstate="collapsed" desc=" AllToAllMin ">                          
    public AllToAllMin(int n, int midX, int midY) {
        n_n = n;
        stageNumber = (int)( java.lang.Math.log10(n_n)/java.lang.Math.log10(2) ) + 1;
        innerSwitch = new InnerSwitch[n_n];
        outerSwitch = new OuterSwitch[n_n];
        switch2by2 = new Switch2By2[n_n][stageNumber];
        connectionLine = new ConnectionLine[n_n][stageNumber-1][2];
        initSwitch(midX, midY);
        initConnectionLine();
        //System.out.println("Stage number: "+stageNumber);
    }// </editor-fold>                        
    
    // <editor-fold defaultstate="collapsed" desc=" initSwitch ">                          
    private void initSwitch(int midX, int midY) {
        int width = Settings.MIN_WIDTH;
        int height = Settings.MIN_HEIGHT;
        int incX = width/(stageNumber-1);
        int incY = height/(n_n-1);
        for(int row = 0; row<n_n; row++) {
            int centerY = midY - height/2 + incY*row;
            int centerX = midX - width/2;
            innerSwitch[row] = new InnerSwitch(centerX, centerY);
            for(int col = 1; col<stageNumber-1; col++) {
                centerX = midX - width/2 + incX*col;
                switch2by2[row][col] = new Switch2By2(centerX, centerY);
            }
            centerX += incX;
//            System.out.println("<CenterX: "+centerX+"> <CenterY: "+centerY+">");
            outerSwitch[row] = new OuterSwitch(centerX, centerY);
        }
    }// </editor-fold>                        
    
    // <editor-fold defaultstate="collapsed" desc=" initConnectionLine ">                          
    private void initConnectionLine() {
        int row, col;
        int nextRow;
        for(col=1; col<stageNumber-2; col++) {
            for(row=0; row<n_n; row++) {
                //System.out.printf("d:%d %d %d %d\n", row, col, (row+col*Math.pow(2, col)), (col+1));
                nextRow = row;
                connectionLine[row][col][0] = new ConnectionLine(
                        switch2by2[row][col].getOutputPoint(0),
                        switch2by2[nextRow][col+1].getInputPoint(0));
                nextRow = (row+(int)Math.pow(2, col)) % n_n;
                connectionLine[row][col][1] = new ConnectionLine(
                        switch2by2[row][col].getOutputPoint(1),
                        switch2by2[nextRow][col+1].getInputPoint(1));
            }
        }
        
        for (row = 0; row < n_n; row++) {
            connectionLine[row][0][0] = new ConnectionLine(
                    innerSwitch[row].getOutputPoint(0),
                    switch2by2[row][1].getInputPoint(0));
            connectionLine[row][0][1] = new ConnectionLine(
                    innerSwitch[row].getOutputPoint(1),
                    switch2by2[(row+1)%n_n][1].getInputPoint(1));
            //System.out.println("Row>"+row+" Col>"+col);
            connectionLine[row][stageNumber-2][0] = new ConnectionLine(
                    switch2by2[row][stageNumber-2].getOutputPoint(0),
                    outerSwitch[row].getInputPoint(0));
            connectionLine[row][stageNumber-2][1] = new ConnectionLine(
                    switch2by2[row][stageNumber-2].getOutputPoint(1),
                    outerSwitch[(row+(int)Math.pow(2, stageNumber-2))%n_n].getInputPoint(1));
        }
    }// </editor-fold>                        
    
    // <editor-fold defaultstate="collapsed" desc=" drawMIN ">                          
    public void drawMIN(Graphics graphics) {
        for(int row = 0; row<n_n; row++) {
            innerSwitch[row].drawSwitch(graphics);
            outerSwitch[row].drawSwitch(graphics);
            for(int col = 1; col<stageNumber-1; col++) {
                switch2by2[row][col].drawSwitch(graphics);
            }
        }
        
        Graphics2D graphics2D = (Graphics2D) graphics;
        for(int row=0; row<n_n; row++)
            for(int col=0; col<stageNumber-1; col++) {
                graphics2D.draw(connectionLine[row][col][0]);
                graphics2D.draw(connectionLine[row][col][1]);
            }
    }// </editor-fold>                        
    
    // <editor-fold defaultstate="collapsed" desc=" getInputPoint ">                          
    public Point getInputPoint(int inputNo) {
        return innerSwitch[inputNo].getInputPoint();
    }// </editor-fold>                        

    // <editor-fold defaultstate="collapsed" desc=" getOutputPoint ">                          
    public Point getOutputPoint(int outputNo) {
        return outerSwitch[outputNo].getOutputPoint();
    }// </editor-fold>                        
    
    public void routeAllToAllMIN(int route) {
//        System.out.print("[ Shift: "+route+" => ");
        int binaryRoute[] = new int[stageNumber];
        for (int i=0; i<stageNumber-1; i++) {
            binaryRoute[i] = route % 2;
            route /= 2;
//            System.out.print(binaryRoute[i]+" ");
        }
//        System.out.println("]");
        /*********************** Set color for first stage **************/
        for (int row=0; row<n_n; row++) {
            switch2by2[(row+binaryRoute[0])%n_n][1].setColor(innerSwitch[row].getColor());
        }
        for(int col = 2; col<stageNumber-1; col++) {
            int shift = (int)Math.pow(2, col-1)*binaryRoute[col-1];
            for(int row=0; row<n_n; row++)
                switch2by2[(row+shift)%n_n][col].setColor(switch2by2[row][col-1].getColor());
        }
        
        int shift = (int)Math.pow(2, stageNumber-2)*binaryRoute[stageNumber-2];
//        System.out.println("<"+shift+">["+binaryRoute[stageNumber-2]+"]<"+stageNumber+">");
        for(int row=0; row<n_n; row++) {
            outerSwitch[(row+shift)%n_n].setColor(switch2by2[row][stageNumber-2].getColor());
        }
    }

    public Color getOutputPointColor(int i) {
        return outerSwitch[i].getColor();
    }

    public void setInputPointColor(int i, Color color) {
        innerSwitch[i].setColor(color);
    }

    public void routeData(Data data, int stage) {
        int source = data.getSourcePort(stage) % n_n;
        int route = data.getRoute(stage);
        int binaryRoute[] = new int[stageNumber];
        int i = 0;
        while(route != 0) {
            binaryRoute[i] = route % 2;
            route /= 2;
            i++;
        }
        int shift = binaryRoute[0];
        Color color = data.getColor();
        innerSwitch[source].setColor(color);
        for(i=1; i<stageNumber-1; i++) {
            switch2by2[(source + shift) % n_n][i].setColor(color);
            shift += (int) Math.pow(2, i) * binaryRoute[i];
        }
        shift += (int) Math.pow(2, stageNumber-1) * binaryRoute[stageNumber-1];
        outerSwitch[(source + shift) % n_n].setColor(color);
    }

    void refresh() {
        for (int i = 0; i < n_n; i++) {
            innerSwitch[i].setColor(Settings.DEFAULT_COLOR);
            outerSwitch[i].setColor(Settings.DEFAULT_COLOR);
            for (int j = 1; j < stageNumber-1; j++) {
                switch2by2[i][j].setColor(Settings.DEFAULT_COLOR);
            }
        }
    }
    
    private final int n_n;
    private final int stageNumber;
    private InnerSwitch innerSwitch[];
    private Switch2By2 switch2by2[][];
    private OuterSwitch outerSwitch[];
    private ConnectionLine connectionLine[][][];
    
}
